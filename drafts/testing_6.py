# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 11:55:25 2022

@author: Julian
"""

import sys
import json
import os
import pickle

sys.path.append(r"C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts")
from functions_pickthebest import create_url, get_bs_file, get_list_hotels_in_page,get_total_number_properties
import numpy as np
import pandas as pd

country = 'Belgium'#'Switzerland'#'United States'#'Spain'#'Mexico'#'United Kingdom'#'Scotland'#'Portugal' #'Spain'
city = ''#'Barcelona'#'London'#'Cancun'#'Tenerife'
datein = '2022-09-01'
dateout= '2022-09-10'
number_elements_page = 25
max_elements_retrieved = 150

path_out = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files'

#countryid = countryid if countryid[-1] != ' ' else countryid[:-1]
#cityid = cityid if cityid[-1] != ' ' else cityid[:-1]
#path_location = os.path.join(path_out,countryid,cityid)

if len(city) == 0:
    path_location = os.path.join(path_out,country)
else:
    path_location = os.path.join(path_out,country,city)

os.makedirs(path_location, exist_ok=True)

url = create_url( group_adults = 2,
                group_children=1,
                country = country, 
                city = city, 
                datein = datein, 
                dateout= dateout, 
                offset = 1,
                language = 'en-gb',
                mealplan = None,
                hotels_id = None,
                order = 'bayesian_review_score')
# order = 'score_and_price')

soup, page_text = get_bs_file(url)
number_properties_found =  get_total_number_properties(soup)
max_elements_retrieved = number_properties_found
print(max_elements_retrieved)
if max_elements_retrieved > 1000:
    max_elements_retrieved = 1000

number_requested_offsets = round(max_elements_retrieved / number_elements_page)
list_offsets = np.array((np.arange(number_requested_offsets))*number_elements_page)

dict_all_hotels = {}
dict_all_hotels_data = {}
for loop in list_offsets:
    url = create_url( group_adults = 2,
                    group_children=1,
                    country = country, 
                    city = city, 
                    datein = datein, 
                    dateout= dateout, 
                    offset = loop,
                    language = 'en-gb',
                    mealplan = None,
                    hotels_id = None,
                    order = 'score_and_price')
    


    soup, page_text = get_bs_file(url)

    all_scripts =  soup.find_all('script')
    for number, script in enumerate(all_scripts):
        if 'BasicPropertyData' in script.text:
            text_script = script.text
            data = json.loads(text_script)
            break

    list_valid_keys = [x for x in data.keys() if 'BasicPropertyData' in x ]
    data_valid = dict( ((key, data[key]) for key in list_valid_keys) )
    data_valid_data = dict( ((key, data) for key in list_valid_keys) )
    
    dict_all_hotels.update(data_valid)
    dict_all_hotels_data.update(data_valid_data)
    
    
valid_keys = list(set(dict_all_hotels.keys()))

dict_hotel_facilities = {}
df_pricing = pd.DataFrame(dtype=float)

for nb_iter, i_hotels in enumerate(valid_keys):
    print('\n' + str(nb_iter) + ' / '+ str(len(valid_keys)))
    
    data_valid_hotel = dict_all_hotels[i_hotels]
    data_valid_hotel_data = dict_all_hotels_data[i_hotels]
    
    pagename_hotel = data_valid_hotel['pageName']
    hotelid = data_valid_hotel['id']
    accommodationTypeId = data_valid_hotel['accommodationTypeId']
    countryid = data_valid_hotel['location']['countryCode']
    cityid = data_valid_hotel['location']['city']
    
    path_hotel = os.path.join(path_location,str(accommodationTypeId)+'_'+str(hotelid)+'_'+pagename_hotel+'.json')

    if not os.path.exists(path_hotel):
        try:
            reviews_totalScore = data_valid_hotel['reviews']['totalScore']
        except:
            reviews_totalScore = float('NaN')
        try:
            reviews_reviewsCount = data_valid_hotel['reviews']['reviewsCount']
        except:
            reviews_reviewsCount = float('NaN')    
        try:
            externalReviews_totalScore = data_valid_hotel['externalReviews']['totalScore']
        except:
            externalReviews_totalScore = float('NaN')
        try:    
            externalReviews_reviewsCount = data_valid_hotel['externalReviews']['reviewsCount']
        except:
            externalReviews_reviewsCount = float('NaN')
        try:
            starRating = data_valid_hotel['starRating']['value']
        except:
            starRating = float('NaN')    
        
        df_highlevel_specs = pd.Series(dtype=float)
        df_highlevel_specs['pagename_hotel'] = pagename_hotel
        df_highlevel_specs['hotelid'] = hotelid
        df_highlevel_specs['accommodationTypeId'] =accommodationTypeId 
        df_highlevel_specs['countryid'] = countryid
        df_highlevel_specs['cityid'] = cityid
        df_highlevel_specs['reviews_totalScore'] = reviews_totalScore
        df_highlevel_specs['reviews_reviewsCount'] = reviews_reviewsCount
        df_highlevel_specs['externalReviews_totalScore'] = externalReviews_totalScore
        df_highlevel_specs['externalReviews_reviewsCount'] = externalReviews_reviewsCount
        df_highlevel_specs['starRating'] = starRating
        
        
        data = dict_all_hotels_data[i_hotels]
        list_keys = list(data['ROOT_QUERY'].keys())
        list_keys = [x for x in list_keys if x != '__typename']
        
        result2 = data['ROOT_QUERY'][list_keys[0]]['results']
        for hotel_data in result2:
            id_hotel = hotel_data['basicPropertyData']['__ref']
            id_hotel = int(''.join(i for i in id_hotel if i.isdigit()))
    
            pricing_hotel = hotel_data['blocks'][0]['finalPrice']['amount']
            currency_hotel = hotel_data['blocks'][0]['finalPrice']['currency']
            
            df_pricing.loc[str(id_hotel),'pricing'] = pricing_hotel
            df_pricing.loc[str(id_hotel),'currency'] = currency_hotel
    
        # new_url = 'https://www.booking.com/hotel/es/'+pagename_hotel+'.en-gb.html&checkin='+datein+'&checkout='+dateout
        new_url = 'https://www.booking.com/hotel/'+countryid+'/'+pagename_hotel+'.en-gb.html'
        soup_hotel, page_text_hotel = get_bs_file(new_url)
        fname = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts\ignore_files\page_text_hotel.txt'
        with open(fname, "w", encoding="utf-8") as f:
            f.write(page_text_hotel)
    
        new_url_score = new_url + '#tab-reviews'
        soup_hotel_score, page_text_hotel_score = get_bs_file(new_url_score)
        fname = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts\ignore_files\page_text_hotel_score.txt'
        with open(fname, "w", encoding="utf-8") as f:
            f.write(page_text_hotel_score)
    
    
        all_scripts =  soup_hotel.find('section', 'hotel-facilities')
        
        
        REAL_EXPERIENCES = True if '100% real guest experiences' in  page_text_hotel_score else False
    
        try:
            get_review_title =  soup_hotel_score.find_all('span',  {'class': 'c-score-bar__title'})
            get_review_value =  soup_hotel_score.find_all('span',  {'class': 'c-score-bar__value'})
        
            df_review_score_internal = pd.Series(dtype=float)
            for ix in range(len(get_review_title)):     
                df_review_score_internal.loc[get_review_title[ix].text] = get_review_value[ix]['data-value']
                
            if df_review_score_internal.empty:
                print('no internal review score')
                df_review_score_internal = float('NaN')
    
            else:
                print('\n internal review score')
    
                print(df_review_score_internal)
    
        except:  
            df_review_score_internal = float('NaN')
            print('no internal review score')
        
        try:
            review_score_external = soup_hotel_score.find('span', 
                                                          {'class': 
                                                          'bui-f-font-featured bui-f-color-grayscale external-reviews__based-on'})
            review_score_external = review_score_external.text
            nb_review_score_external = int(''.join(i for i in review_score_external if i.isdigit()))
            
            review_score_external = float(soup_hotel_score.find('div',{'class':'bui-review-score__badge'}).text)
            print('\n external review score')
            print(nb_review_score_external, review_score_external)
            
        except:
            nb_review_score_external = float('NaN')
            review_score_external = float('NaN')
            print('no external review score')
        
        try:
            hotel_facilities = all_scripts.text
            list_facilities = hotel_facilities.split('\n')
            list_facilities = list(filter(None,list_facilities))
            
            # clean facilities
            list_facilities = [ x for x in list_facilities if "Facilities" not in x ]
            list_facilities = [ x for x in list_facilities if "Languages spoken" not in x ]
            list_facilities = [ x for x in list_facilities if "Miscellaneous" not in x ]
            list_facilities = [ x for x in list_facilities if "Reception services" not in x ]
            list_facilities = [ x for x in list_facilities if "Food & Drink" not in x ]
            list_facilities = [ x for x in list_facilities if "Pets" not in x ]
            list_facilities = [ x for x in list_facilities if "Room Amenities" not in x ]
            list_facilities = [ x for x in list_facilities if "Media & Technology" not in x ]
            list_facilities = [ x for x in list_facilities if "Fun for everyone under one roof" not in x ]
            list_facilities = [ x for x in list_facilities if "Bathroom" not in x ]
            list_facilities = [ x for x in list_facilities if "Bedroom" not in x ]
            list_facilities = [ x for x in list_facilities if "Kitchen" not in x ]
            list_facilities = [ x for x in list_facilities if "Freedom to eat when you want" not in x ]
            list_facilities = [ x for x in list_facilities if "Internet" not in x ]
            list_facilities = [ x for x in list_facilities if "Parking" not in x ]
            list_facilities = [ x for x in list_facilities if "See availability" not in x ]
            to_be_removed = ['Most popular facilities','Living Area','Space for everyone to be together',
            'Outdoors','Sit back and relax','Activities','Location & transport',
            'Policies','Pet policies','Outdoor & View','Enjoy the view', 'Cancellation policies',
            'Check-in/check-out times','Couples policies (are non-married individuals allowed?)',
            'Other','I already have a booking at this property','Building characteristics',
            'Entertainment and family services','Brilliant!','Enter your feedback',
            'Back to property','No thanks','Submit','I already have a booking at this property',
            'Thanks for your help!',
            'Your thoughts help us figure out what kind of information we should be asking properties for.',
            'Sorry, but it seems like something went wrong in submitting this.  Would you mind trying again?',
            'Missing some information?','Retry','Yes','/','No', 'Additional charge', 'Free!', 
            'Thanks! You will receive an email as soon as the property has answered your question.',
            'Great! Thanks for your response.',
            'Contacting the property',
            'Breakfast details',
            'Safety & security',
            'Property', 'Pool, spa, and fitness facilities',
            'What topic(s) would you like to know more about?']
            
            for remove_string in to_be_removed:
                list_facilities = [ x for x in list_facilities if remove_string != x ]
            list_facilities = list(set(list_facilities))
            
            print(pagename_hotel + ' : DONE')
            
        
        except AttributeError:
            list_facilities ='NoneType'
            print(pagename_hotel + ' : ERROR')
            
        dict0 = {'list_facilities': list_facilities,
                                                 'hotelid': hotelid,
                                                 'new_url': new_url,
                                                 'pagename_hotel': pagename_hotel,
                                                 'nb_review_score_external': nb_review_score_external,
                                                 'review_score_external': review_score_external,
                                                 'df_review_score_internal': df_review_score_internal,
                                                 'REAL_EXPERIENCES': REAL_EXPERIENCES,
                                                 'df_highlevel_specs': df_highlevel_specs
                                                 }
                
        # path_hotel = os.path.join(path_location,str(accommodationTypeId)+'_'+str(hotelid)+'_'+pagename_hotel+'.json')
        
        with open(path_hotel, 'wb') as fp:
            pickle.dump(dict0, fp)
        


