# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 11:55:25 2022

@author: Julian
"""

import sys
import json

sys.path.append(r"C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts")
from functions_pickthebest import create_url, get_bs_file, get_list_hotels_in_page,get_total_number_properties

country = 'Spain'
city = 'Tenerife'
datein = '2022-07-01'
dateout= '2022-07-08'
 
url = create_url( group_adults = 2,
                group_children=1,
                country = country, 
                city = city, 
                datein = datein, 
                dateout= dateout, 
                offset = 1,
                language = 'en-gb',
                mealplan = None,
                hotels_id = None,
                order = 'score_and_price')

soup, page_text = get_bs_file(url)

all_scripts =  soup.find_all('script')
for number, script in enumerate(all_scripts):
    if 'BasicPropertyData' in script.text:
        text_script = script.text
        data = json.loads(text_script)
        break

list_valid_keys = [x for x in data.keys() if 'BasicPropertyData' in x ]
data_valid = dict( ((key, data[key]) for key in list_valid_keys) )
valid_keys = list(data_valid.keys())

dict_hotel_facilities = {}
for i_hotels in valid_keys:
    data_valid_hotel = data_valid[i_hotels]
    pagename_hotel = data_valid_hotel['pageName']
    
    # new_url = 'https://www.booking.com/hotel/es/'+pagename_hotel+'.en-gb.html&checkin='+datein+'&checkout='+dateout
    new_url = 'https://www.booking.com/hotel/es/'+pagename_hotel+'.en-gb.html'
    
    soup_hotel, page_text_hotel = get_bs_file(new_url)
    # fname = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts\ignore_files\page_text_hotel.txt'
    # with open(fname, "w", encoding="utf-8") as f:
    #     f.write(page_text_hotel)

    all_scripts =  soup_hotel.find('section', 'hotel-facilities')
    hotel_facilities = all_scripts.text
    list_facilities = hotel_facilities.split('\n')
    list_facilities = list(filter(None,list_facilities))
    
    # clean facilities
    list_facilities = [ x for x in list_facilities if "Facilities" not in x ]
    list_facilities = [ x for x in list_facilities if "Languages spoken" not in x ]
    list_facilities = [ x for x in list_facilities if "Miscellaneous" not in x ]
    list_facilities = [ x for x in list_facilities if "Reception services" not in x ]
    list_facilities = [ x for x in list_facilities if "Food & Drink" not in x ]
    list_facilities = [ x for x in list_facilities if "Pets" not in x ]
    list_facilities = [ x for x in list_facilities if "Room Amenities" not in x ]
    list_facilities = [ x for x in list_facilities if "Media & Technology" not in x ]
    list_facilities = [ x for x in list_facilities if "Fun for everyone under one roof" not in x ]
    list_facilities = [ x for x in list_facilities if "Bathroom" not in x ]
    list_facilities = [ x for x in list_facilities if "Bedroom" not in x ]
    list_facilities = [ x for x in list_facilities if "Kitchen" not in x ]
    list_facilities = [ x for x in list_facilities if "Freedom to eat when you want" not in x ]
    list_facilities = [ x for x in list_facilities if "Internet" not in x ]
    list_facilities = [ x for x in list_facilities if "Parking" not in x ]
    list_facilities = [ x for x in list_facilities if "See availability" not in x ]
    to_be_removed = ['Most popular facilities','Living Area','Space for everyone to be together',
    'Outdoors','Sit back and relax','Activities','Location & transport',
    'Policies','Pet policies','Outdoor & View','Enjoy the view', 'Cancellation policies',
    'Check-in/check-out times','Couples policies (are non-married individuals allowed?)',
    'Other','I already have a booking at this property','Building characteristics',
    'Entertainment and family services','Brilliant!','Enter your feedback',
    'Back to property','No thanks','Submit','I already have a booking at this property',
    'Thanks for your help!',
    'Your thoughts help us figure out what kind of information we should be asking properties for.',
    'Sorry, but it seems like something went wrong in submitting this.  Would you mind trying again?',
    'Missing some information?','Retry','Yes','/','No', 'Additional charge', 'Free!', 
    'Thanks! You will receive an email as soon as the property has answered your question.',
    'Great! Thanks for your response.',
    'Contacting the property',
    'Breakfast details',
    'Safety & security',
    'Property', 'Pool, spa, and fitness facilities',
    'What topic(s) would you like to know more about?']
    
    for remove_string in to_be_removed:
        list_facilities = [ x for x in list_facilities if remove_string != x ]
    list_facilities = list(set(list_facilities))
    dict_hotel_facilities[pagename_hotel] = list_facilities

    print(pagename_hotel + ' : DONE')