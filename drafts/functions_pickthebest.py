import datetime
import requests
from bs4 import BeautifulSoup
import pandas as pd

REQUEST_HEADER = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36"}
BOOKING_PREFIX = 'https://www.booking.com'
ROW_PER_OFFSET = 1

def get_bs_file(url):
    session = requests.Session()
    response = session.get(url, headers=REQUEST_HEADER)
    page_text = response.text
    soup = BeautifulSoup(page_text, "lxml")
    return soup, page_text

def get_list_hotels_in_page(soup):
    namehotel = soup.find_all('div', {'data-testid':'title'})
    list_hotels_in_page = []
    for ix in namehotel:
        list_hotels_in_page.append(ix.get_text())
    return list_hotels_in_page
    
def get_total_number_properties(soup):
    namelist = soup.find_all('h1', {'class':'e1f827110f d3a14d00da'})
    number_properties_found = namelist[0].get_text()
    number_properties_found = int(''.join(i for i in number_properties_found if i.isdigit()))
    return number_properties_found

def list_languages():

    list_languages = [
                        ["en-gb" ,"English (UK)"],
                        ["en-us" ,"English (US)"],
                        ["de" ,"Deutsch"],
                        ["nl","Nederlands" ],
                        ["fr" ,"Français" ],
                        ["es" ,"Español" ],
                        ["es-ar","Español (AR)" ],
                        ["es-mx","Español (MX)" ],
                        ["ca","Català" ],
                        ["it","Italiano" ],
                        ["pt-pt" ,"Português (PT)" ],
                        ["pt-br","Português (BR)" ],
                        ["no","Norsk" ],
                        ["fi" ,"Suomi" ],
                        ["sv","Svenska" ],
                        ["da","Dansk" ],
                        ["cs","Čeština" ],
                        ["hu","Magyar" ],
                        ["ro","Română" ],
                        ["ja","日本語" ],
                        ["zh-cn","简体中文" ],
                        ["zh-tw","繁體中文" ],
                        ["pl","Polski" ],
                        ["el" ,"Ελληνικά" ],
                        ["ru","Русский" ],
                        ["tr","Türkçe" ],
                        ["bg","Български" ],
                        ["ar","العربية" ],
                        ["ko","한국어" ],
                        ["he","עברית" ],
                        ["lv","Latviski" ],
                        ["uk","Українська" ],
                        ["id","Bahasa Indonesia" ],
                        ["ms","Bahasa Malaysia" ],
                        ["th","ภาษาไทย" ],
                        ["et","Eesti" ],
                        [ "hr","Hrvatski" ],
                        ["lt","Lietuvių" ],
                        ["sk","Slovenčina" ], 
                        ["sr","Srpski" ],
                        ["sl","Slovenščina" ],
                        ["vi" ,"Tiếng Việt" ],
                        ["tl","Filipino" ],
                        ["is","Íslenska" ]]
    return list_languages

def create_url( group_adults = 2,
                group_children=1,
                country = 'Spain', 
                city = '', 
                datein = '2021-07-01', 
                dateout= '2021-07-08', 
                offset = 1,
                language = 'en-gb',
                mealplan = None,
                hotels_id = None,
                order = 'score_and_price'):

    if isinstance(datein, str) or isinstance(dateout, str):
        datein = datetime.datetime.strptime(datein, "%Y-%m-%d")
        dateout = datetime.datetime.strptime(dateout, "%Y-%m-%d")

    ####
    if mealplan == None:
        mealplan_str = ''
    elif mealplan == 'All-Inclusive':
        mealplan_str = 'mealplan%3D4%3B'

    if hotels_id == None:
        hotels_id_str = ''
    elif hotels_id == 'Hotels':
        hotels_id_str = 'ht_id%3D204%3B'

    if (mealplan != None) or (hotels_id != None):
        filters = '&nflt=' + hotels_id_str + mealplan_str
    else:
        filters = ''
    ####
    if order == None:
        order_str = 'price'
    elif order == 'score_and_price':
        order_str = 'review_score_and_price'
    else:
        order_str = order


    url = "https://www.booking.com/searchresults.{in_language}.html?checkin_month={in_month}" \
        "&checkin_monthday={in_day}&checkin_year={in_year}&checkout_month={out_month}" \
        "&checkout_monthday={out_day}&checkout_year={out_year}&group_adults={group_adults}&group_children={group_children}" \
        "&order={order_str}&ss={city}%2C%20{country}&offset={offset}{mealplan_str}"\
        .format(in_month=str(datein.month),
                in_day=str(datein.day),
                in_year=str(datein.year),
                out_month=str(dateout.month),
                out_day=str(dateout.day),
                out_year=str(dateout.year),
                in_language=str(language),
                group_adults=group_adults,
                group_children=group_children,
                city=city,
                country=country,
                offset=offset,
                mealplan_str = mealplan_str,
                order_str = order_str)
    url = url + filters
    #if only_hotels:
    #    url = url + '&percent_htype_hotel=1'
    return url

def get_max_offset(soup):
    all_offset = []
    if soup.find_all('li', {'class': 'sr_pagination_item'}) is not None:
        all_offset = soup.find_all('li', {'class': 'sr_pagination_item'})[-1].get_text().splitlines()[-1]

    return all_offset

def get_list_hotels_from_url(starting_url):
    session = requests.Session()
    #today = datetime.datetime.now()
    #tomorrow = today + datetime.timedelta(1)

    response = session.get(starting_url, headers=REQUEST_HEADER)
    soup = BeautifulSoup(response.text, "lxml")

    hotels = soup.select("#hotellist_inner  div.sr_item.sr_item_new")
    max_offset = get_max_offset(soup)
    return hotels,max_offset

def get_hotel_main_info(hotel): 
    hotel_info = {}
    hotel_info['name'] = get_hotel_name(hotel)
    hotel_info['score'] = get_hotel_score(hotel)
    hotel_info['price'] = get_hotel_price(hotel)
    hotel_info['link'] = get_hotel_detail_link(hotel)
    return hotel_info

def get_hotel_details(hotel_info):
    details = {}
    session = requests.Session()
    detail_page_response = session.get(BOOKING_PREFIX + hotel_info['link'], headers=REQUEST_HEADER)
    soup_detail = BeautifulSoup(detail_page_response.text, "lxml")
    details['latitude'] = get_coordinates(soup_detail)[0]
    details['longitude'] = get_coordinates(soup_detail)[1]
    details['important_facilities'] = get_important_facilites(soup_detail)
    details['neighborhood_structures'] = get_neighborhood_structures(soup_detail)
    details['services_offered'] = get_services_offered(soup_detail)
    return details
##########################


def get_hotel_name(hotel):
    if hotel.select_one("span.sr-hotel__name") is None:
        return ''
    else:
        return hotel.select_one("span.sr-hotel__name").text.strip()

def get_hotel_score(hotel):
    if hotel.select_one("div.bui-review-score__badge") is None:
        return ''
    else:
        return hotel.select_one("div.bui-review-score__badge").text.strip()

def get_hotel_price(hotel):
    if hotel.select_one("div.bui-price-display__value.prco-inline-block-maker-helper") is None:
        return ''
    else:
        return hotel.select_one("div.bui-price-display__value.prco-inline-block-maker-helper").text.strip()[2:]

def get_hotel_detail_link(hotel):
    if hotel.select_one(".txp-cta.bui-button.bui-button--primary.sr_cta_button") is None:
        return ''
    else:
        return hotel.select_one(".txp-cta.bui-button.bui-button--primary.sr_cta_button")['href']
def get_coordinates(soup_detail):
    coordinates = []
    if soup_detail.select_one("#hotel_sidebar_static_map") is None:
        coordinates.append('')
        coordinates.append('')
    else:
        coordinates.append(soup_detail.select_one("#hotel_sidebar_static_map")["data-atlas-latlng"].split(",")[0])
        coordinates.append(soup_detail.select_one("#hotel_sidebar_static_map")["data-atlas-latlng"].split(",")[1])

    return coordinates


def get_important_facilites(soup_detail):

    if soup_detail.select_one("div.hp_desc_important_facilities.clearfix.hp_desc_important_facilities--bui") is None:
        return []
    else:
        return list(dict.fromkeys([service.text.strip() for service in soup_detail.findAll("div", {"class": "important_facility"})]))

def get_neighborhood_structures(soup_detail):

    neighborhood_list = []

    if soup_detail.select_one('div.hp-poi-content-container.hp-poi-content-container--column.clearfix') is None:
        neighborhood_list = []
    else:

        for neighborhood in soup_detail.select_one('div.hp-poi-content-container.hp-poi-content-container--column.clearfix').findAll('li', {"class": "bui-list__item"}):
            neighborhood_structures = {}

            if neighborhood.find("div", {"class": "hp-poi-list__description"}).contents[0].strip() is '':
                neighborhood_structures['name'] = neighborhood.find("div", {"class": "hp-poi-list__description"}).span.text.strip()
            else:
                neighborhood_structures['name'] = neighborhood.find("div", {"class": "hp-poi-list__description"}).contents[0].strip()

            try:
                # print(str(neighborhood.find("div", {"class":"hp-poi-list__description"}).select_one("span.bui-badge.bui-badge--outline").text.strip()))
                neighborhood_structures['structure_type'] = neighborhood.find("div", {"class": "hp-poi-list__body"}).select_one("span.bui-badge.bui-badge--outline").text.strip()
            except:
                neighborhood_structures['structure_type'] = ''

            try:
                neighborhood_structures['distance'] = neighborhood.find('span', {"class": "hp-poi-list__distance"}).text.strip()
            except:
                neighborhood_structures['distance'] = ''

            neighborhood_list.append(neighborhood_structures)
    
    return neighborhood_list


def get_services_offered(soup_detail):

    services_offered_list = []

    if soup_detail.select_one('div.facilitiesChecklist') is None:
        services_offered_list = []
    else:

        for services in soup_detail.findAll("div", class_="facilitiesChecklistSection"):

            services_offered = {}
            services_offered['type'] = services.find("h5").text.strip()

            services_offered['value'] = []
            for checks in services.findAll("li"):

                if checks.find("p") is not None:
                    services_offered['value'].append(checks.findNext("p").text.strip().replace("\n", " ").replace("\r", " ").replace("  ", " "))

                elif checks.find("span") is not None:
                    services_offered['value'].append(checks.find("span").text.strip())
                else:
                    services_offered['value'].append(checks.text.strip())

            services_offered_list.append(services_offered)
    
    return services_offered_list
