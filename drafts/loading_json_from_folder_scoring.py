# -*- coding: utf-8 -*-
"""
Created on Sun Apr 10 17:14:13 2022

@author: Julian
"""

import os
import pickle
import glob
import pandas as pd

path_scores = r'C:/Users/Julian/Downloads/list_facilities.xlsx'
df_score = pd.read_excel(path_scores, index_col = 0 )

# path_r = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files\Mexico\Cancun/*'
path_r = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files/**/*.json'
hotels_data = glob.glob(path_r,recursive=True)

df_scoring = pd.DataFrame()

for ix, hotel_data in enumerate(hotels_data):
    df_score_hotel = df_score.copy()
    df_score_hotel.index = df_score_hotel['Unnamed: 1']
    del df_score_hotel['Unnamed: 1']
    df_score_hotel['EXISTS'] = False
    with open(hotel_data, 'rb') as fp:
        data = pickle.load(fp)
        
        path_country = hotel_data.split('\\')[-3]
        if path_country == 'downloaded_files':
            path_country = hotel_data.split('\\')[-2]
            path_city = float('NaN')
        else:
            path_city = hotel_data.split('\\')[-2]
        
        df_highlevel_specs = pd.DataFrame(data['df_highlevel_specs']).T
        df_highlevel_specs['new_url'] = data['new_url']
        df_highlevel_specs['hotelid'] = data['hotelid']
        df_highlevel_specs['REAL_EXPERIENCES'] = data['REAL_EXPERIENCES']
        df_highlevel_specs['PATH_COUNTRY'] = path_country
        df_highlevel_specs['PATH_CITY'] = path_city
        df_highlevel_specs.index = [ix]
        
        list_facilities = data['list_facilities']
        list_facilities = [x for x in list_facilities if ('arking' not in x) &
                           ('WiFi' not in x) &
                           ('Wired internet' not in x) &
                           ('Great facilities!'not in x) &
                           ('facilities' not in x) & (len(x) > 1)]
        
        if not isinstance(list_facilities,str):
            for facility in list_facilities:
                
                df_score_hotel.loc[facility,'EXISTS'] = True
                
        # FILTER THE MATCHING FACILITIES
        df_score_hotel_filter = df_score_hotel[df_score_hotel['EXISTS'] == True]        
        df_score_hotel_filter = df_score_hotel_filter[
            (df_score_hotel_filter['Kid activity']>0) |
            (df_score_hotel_filter['Family activ']>0) |
            (df_score_hotel_filter['Score'] >0) 
            ]
        
        # outdoor (kids)
        # indoor (kids)
        #////////////////////////
        
        df_score_hotel_filter_sum = df_score_hotel_filter.sum(axis=0)
        
        df_score_hotel_filter_sum = pd.DataFrame(df_score_hotel_filter_sum).T
        df_score_hotel_filter_sum.index = [ix]
        
        df_highlevel_specs = pd.concat([df_highlevel_specs, df_score_hotel_filter_sum], axis = 1)

        df_scoring = pd.concat([df_scoring, df_highlevel_specs], axis=0)
        
df_scoring_high_reviews = df_scoring[df_scoring['reviews_reviewsCount'] > 500]

# print(list(set(df_scoring['accommodationTypeId'])))
# print(len(list(set(df_scoring['accommodationTypeId']))))


# list_facilities_all_final = list(set(list_facilities_all))

# list_facilities_all_final = pd.Series(list_facilities_all_final)
# list_facilities_all_final.to_csv(r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files\list_facilities.csv')