# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 14:27:02 2022

@author: Julian

https://[domain]/[version]/[xml or json]/[endpoint]?[parameters]

The offset and rows input parameters are used for pagination.
The offset input parameter specifies where in the resulting list to start pagination.
The rows parameter specifies the maximum number of entries to return.

Retrieve the first 100 entries: cities?offset=0&rows=100
Retrieve the next 100 entries starting from the 100th row: cities?offset=100&rows=100


Destination Endpoints

/countries
/cities
/districts
/regions

Reference Endpoints
/hotelFacilityTypes
/hotelTypes
/chainTypes
/paymentTypes

Using /changedHotels and /hotels Endpoints

Availability Endpoints
/hotelAvailability
/blockAvailability

Recommended Update Frequency
All availability endpoints should be used in real-time and never cached. The other endpoints can be used less frequently.

Once per day:

/changedHotels to receive a list of properties with changed data
/hotels to update the changed hotels
Once per week:

Types endpoints such as /hotelFacilityTypes and /hotelTypes
Destination endpoints such as /countries and /cities


"""

import sys
sys.path.append(r"C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts")
from functions_pickthebest import create_url, get_list_hotels_from_url
import datetime
import requests
from bs4 import BeautifulSoup
import pandas as pd

REQUEST_HEADER = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36"}
BOOKING_PREFIX = 'https://www.booking.com'
ROW_PER_OFFSET = 1


url = create_url( group_adults = 2,
                group_children=1,
                country = 'Spain', 
                city = '', 
                datein = '2022-07-01', 
                dateout= '2022-07-08', 
                offset = 1,
                language = 'en-gb',
                mealplan = None,
                hotels_id = None,
                order = 'score_and_price')

session = requests.Session()
#today = datetime.datetime.now()
#tomorrow = today + datetime.timedelta(1)

response = session.get(url, headers=REQUEST_HEADER)
soup = BeautifulSoup(response.text, "lxml")



soup = BeautifulSoup(response.text, "lxml")

hotels = soup.select("#hotellist_inner  div.sr_item.sr_item_new")


hotels,max_offset = get_list_hotels_from_url(url)








from bs4 import BeautifulSoup
import requests

headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'}

aid=1510190
offset = 0
rows = 100

query = 'cities?offset='+str(offset)+'&rows='+str(rows)

main_url = 'https://www.booking.com/'
language = 'en-us'
checkin = '2022-04-29'
checkout = '2022-04-30'

generate_url = main_url + 'searchresults.'+language+'.html?aid='+str(aid)+\
        'checkin='+checkin+';checkout='+checkout+';city=-373226;highlighted_hotels=90831;hlrd=with_av;keep_landing=1;no_rooms=1;redirected=1;source=hotel&room1=A,A,;'


# GETTING THE HTML CODE
# url = 'https://www.booking.com/searchresults.html?label=gen173nr-1DCAEoggI46AdIM1gEaMkBiAEBmAExuAEXyAEM2AED6AEB-AECiAIBqAIDuAL0uO6RBsACAdICJDFiODNlNmQ1LTY1Y2MtNGM1My05NzZhLTVjMzQzMTAwNjJjZtgCBOACAQ&sid=e252c08e8b7811d4324f61c3862cd5c7&sb=1&sb_lp=1&src=index&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Findex.html%3Flabel%3Dgen173nr-1DCAEoggI46AdIM1gEaMkBiAEBmAExuAEXyAEM2AED6AEB-AECiAIBqAIDuAL0uO6RBsACAdICJDFiODNlNmQ1LTY1Y2MtNGM1My05NzZhLTVjMzQzMTAwNjJjZtgCBOACAQ%3Bsid%3De252c08e8b7811d4324f61c3862cd5c7%3Bsb_price_type%3Dtotal%26%3B&ss=Rengo&is_ski_area=0&ssne=Rengo&ssne_untouched=Rengo&dest_id=-899047&dest_type=city&checkin_year=2022&checkin_month=4&checkin_monthday=12&checkout_year=2022&checkout_month=4&checkout_monthday=13&group_adults=2&group_children=0&no_rooms=1&b_h4u_keep_filters=&from_sf=1'
response=requests.get(generate_url,headers=headers)

# USING BS FOR PARSING THE CONTENT
soup=BeautifulSoup(response.content,'lxml')

# LOOKING FOR THE INFORMATION
reviews = soup.findAll("script",'BasicPropertyData')
for review in reviews:
    print(review)
    break


    
    
