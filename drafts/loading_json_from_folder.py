# -*- coding: utf-8 -*-
"""
Created on Sun Apr 10 17:14:13 2022

@author: Julian
"""

import os
import pickle
import glob
import pandas as pd

# path_r = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files\Mexico\Cancun/*'
path_r = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files/**/*.json'
hotels_data = glob.glob(path_r,recursive=True)

df_scoring = pd.DataFrame()


list_facilities_all = []
for ix, hotel_data in enumerate(hotels_data):
    with open(hotel_data, 'rb') as fp:
        data = pickle.load(fp)
        
        df_highlevel_specs = pd.DataFrame(data['df_highlevel_specs']).T
        df_highlevel_specs['new_url'] = data['new_url']
        df_highlevel_specs['hotelid'] = data['hotelid']
        df_highlevel_specs['REAL_EXPERIENCES'] = data['REAL_EXPERIENCES']
        df_highlevel_specs.index = [ix]
        df_scoring = pd.concat([df_scoring, df_highlevel_specs], axis=0)
        
        list_facilities = data['list_facilities']
        list_facilities = [x for x in list_facilities if ('arking' not in x) &
                           ('WiFi' not in x) &
                           ('Wired internet' not in x) &
                           ('Great facilities!'not in x) &
                           ('facilities' not in x) & (len(x) > 1)]
        
        if not isinstance(list_facilities,str):
            list_facilities_all = list_facilities_all + list_facilities
            list_facilities_all = list(set(list_facilities_all))

df_scoring_high_reviews = df_scoring[df_scoring['reviews_reviewsCount'] > 500]

print(list(set(df_scoring['accommodationTypeId'])))
print(len(list(set(df_scoring['accommodationTypeId']))))


list_facilities_all_final = list(set(list_facilities_all))

list_facilities_all_final = pd.Series(list_facilities_all_final)
list_facilities_all_final.to_csv(r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\downloaded_files\list_facilities.csv')