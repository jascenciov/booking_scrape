# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 10:14:28 2022

@author: Julian
"""


import sys
sys.path.append(r"C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts")
from functions_pickthebest import create_url, get_bs_file, get_list_hotels_in_page,get_total_number_properties

country = 'Spain'
city = 'Tenerife'
 
url = create_url( group_adults = 2,
                group_children=1,
                country = country, 
                city = city, 
                datein = '2022-07-01', 
                dateout= '2022-07-08', 
                offset = 1,
                language = 'en-gb',
                mealplan = None,
                hotels_id = None,
                order = 'score_and_price')

soup, page_text = get_bs_file(url)

fname = r'C:\Users\Julian\Documents\coding_virtualenvs\booking_scrape\drafts\ignore_files\page_text.txt'
with open(fname, "w", encoding="utf-8") as f:
    f.write(page_text)

# CHECK NUMBER OF PROPERTIES
number_properties_found = get_total_number_properties(soup)

print('Properties in ' +country+ ' = ' + str(number_properties_found))

# GET LIST OF HOTELS IN CURRENT PAGE
list_hotels_in_page = get_list_hotels_in_page(soup)

soup_name = soup.find_all('div', {'data-testid':'title'})
list_hotels_in_page = []
for ix in soup_name:
    list_hotels_in_page.append(ix.get_text())
    
soup_price = soup.find_all('span', {'class':'fcab3ed991 bd73d13072'})
list_price_in_page = []
for ix in soup_price:
    price = int(''.join(i for i in ix.get_text() if i.isdigit()))
    list_price_in_page.append(price)
    
links = []
hrefs = soup.find_all('a', href=True)
for href in hrefs:
    href_page = href['href']
    if ('booking.com/hotel' in href_page) & ('hotelTmpl' in href_page):
        links.append(href_page)
    
links = list(set(links))

pageNames = soup.find('BasicPropertyData')

pagename_text = pageNames[50].get_text()

import json

all_scripts =  soup.find_all('script')
for number, script in enumerate(all_scripts):
    if 'BasicPropertyData' in script.text:
        text_script = script.text
        data = json.loads(text_script)
        break

list_valid_keys = [x for x in data.keys() if 'BasicPropertyData' in x ]
data_valid = dict( ((key, data[key]) for key in list_valid_keys) )


dict_script = eval(text_script)

import pandas as pd

df_hotels = pd.DataFrame(data= [list_hotels_in_page,
                                list_price_in_page,
                                links]).T
df_hotels.columns = ['name','first_price','links']


